package com.tinmegali.mylocation;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class CountOfCustomer extends AppCompatActivity {
    TextView notifiyCount;
    TextView IntrestedCount;
    FirebaseFirestore Firedb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_count_of_customer);

        Firedb = FirebaseFirestore.getInstance();
        notifiyCount =  findViewById(R.id.notifiyCount);
        IntrestedCount =  findViewById(R.id.IntrestedCount);
        ReadCount();
    }

    private void ReadCount()
    {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        Firedb.collection("users").document(user.getUid().toString()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @SuppressLint("SetTextI18n")
            @Override public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                DocumentSnapshot document = task.getResult();
                Log.e("ajay", "onComplete: "+document.get("count")+document.get("VisitedCount") );
                notifiyCount.setText ("Count of Notifications..!!\n\n"+(String) document.get("count"));
                IntrestedCount.setText("Count of Interested Customers..!!\n\n"+(String) document.get("VisitedCount"));

                 }
});
    }
}