package com.tinmegali.mylocation;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofenceStatusCodes;
import com.google.android.gms.location.GeofencingEvent;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static java.util.UUID.randomUUID;


public class GeofenceTrasitionService extends IntentService {

    private List<Geofence> geofenceList;
    private List<Marker> markerList;
    private List<Circle> circleList;
    StorageReference storageReference;
    FirebaseStorage storage;
    FirebaseUser user;
    Bitmap bitmap;
    FirebaseFirestore Firedb;

    public List<Geofence> getGeofenceList() {
        return geofenceList;
    }

    public void setGeofenceList(List<Geofence> geofenceList) {
        this.geofenceList = geofenceList;
    }

    public List<Marker> getMarkerList() {
        return markerList;
    }

    public void setMarkerList(List<Marker> markerList) {
        this.markerList = markerList;
    }

    public List<Circle> getCircleList() {
        return circleList;
    }

    public void setCircleList(List<Circle> circleList) {
        this.circleList = circleList;
    }

    public void addMarker(Marker marker){
        markerList.add(marker);
        Log.e("recovercheck", "added marker , new size : "+markerList.size());
    }


    public void addCircle(Circle marker){
        circleList.add(marker);
        Log.e("recovercheck", "added circle , new size : "+circleList.size());
    }

    public void addGeofence(Geofence marker){
        geofenceList.add(marker);
        Log.e("recovercheck", "added geofence , new size : "+geofenceList.size());
    }

    private NotificationManager notificatioMng;

    private static final String TAG = GeofenceTrasitionService.class.getSimpleName();

    public static final int GEOFENCE_NOTIFICATION_ID = 0;
  //  DataBaseHelper mydb;

    public GeofenceTrasitionService() {
        super(TAG);
    //    mydb = new DataBaseHelper(this);
        geofenceList =  new ArrayList<>();
        markerList =  new ArrayList<>();
        circleList =  new ArrayList<>();
        Log.e("recovercheck", "intent service created");
        storage = FirebaseStorage.getInstance();
        user = FirebaseAuth.getInstance().getCurrentUser();
        storageReference = storage.getReference();
        Firedb = FirebaseFirestore.getInstance();


    }

    @Override
    protected void onHandleIntent(Intent intent) {
        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        boolean state=true;

        // Handling errors
        if (geofencingEvent.hasError()) {
            String errorMsg = getErrorString(geofencingEvent.getErrorCode());
            Log.e(TAG, errorMsg);
            return;
        }

        int geoFenceTransition = geofencingEvent.getGeofenceTransition();

        // Check if the transition type is of interest
        if (geoFenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER ||
                geoFenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT )
        {
            // Get the geofence that were triggered

            List<Geofence> triggeringGeofences = geofencingEvent.getTriggeringGeofences();

            Log.e("ajay ","size of exit: "+triggeringGeofences.size() );
            String geofenceTransitionDetails = getGeofenceTrasitionDetails(geoFenceTransition, triggeringGeofences);
            if(geoFenceTransition==Geofence.GEOFENCE_TRANSITION_EXIT)
            {
                for(int i=0;i<triggeringGeofences.size();i++)
                {
                    geoFenceTriggerid=triggeringGeofences.get(i).getRequestId();
                    try {
                        sendNotification("exit",false);
                        RemoveShopFromDB(geoFenceTriggerid);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }
            else
            {
            Log.d("ajay2", "inside notify ");
            // Send notification details as a String
            try {
                sendNotification(geofenceTransitionDetails,state);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
            }

    }

    public void addShopToDb(String msg, Bitmap bitmap, String geoFenceTriggerid)
    {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        String curr = simpleDateFormat.format(new Date());
        LatLng latLng = MainActivity.method();
        if (latLng==null)
            latLng=ShopkeeperActivity.method();
        LogModel logModel = new LogModel();
        logModel.setDateTime(curr);
        logModel.setLati(latLng.latitude);
        logModel.setLongi(latLng.longitude);
        logModel.setGeoFenceId(geoFenceTriggerid);
        logModel.setTransiion(msg);
        logModel.setByteArray(byteArray);
        Firedb.collection("users").document(geoFenceTriggerid).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                DocumentSnapshot document = task.getResult();
                String group = (String) document.get("ShopType");
                Log.d("ajay", "Shop Type" + group);
                logModel.setShopType(group);
                logModel.save();

            }
        });


    }
    public void RemoveShopFromDB(String geoFenceTriggerid)
    {
        List<LogModel> notes = LogModel.findWithQuery(LogModel.class, "SELECT * FROM LOG_MODEL WHERE geo_fence_id = ?", geoFenceTriggerid);
        LogModel.deleteInTx(notes);
    }

    String geoFenceTriggerid;
    private String getGeofenceTrasitionDetails(int geoFenceTransition, List<Geofence> triggeringGeofences) {

        // get the ID of each geofence triggered
        ArrayList<String> triggeringGeofencesList = new ArrayList<>();
        for (Geofence geofence : triggeringGeofences) {

            geoFenceTriggerid = geofence.getRequestId();
            triggeringGeofencesList.add(geofence.getRequestId());
        }

        String status = null;
        if (geoFenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER)
        {
            status="enter";


        } else if (geoFenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {

            //no need to save after exiting try removing the record for entering if user exits this geofence also
            status="exit";


        }

        return status;

    }

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat();

    private void sendNotification(String msg,boolean state) throws IOException {
        // Intent to start the main Activity
        Intent notificationIntent = MainActivity.makeNotificationIntent(
                getApplicationContext(), msg
        );

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(notificationIntent);
        PendingIntent notificationPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);


        // Creating and sending Notification
        notificatioMng = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Log.e("ajay", "sendNotification: "+geoFenceTriggerid );
        StorageReference mImageRef = storage.getReference("images/"+geoFenceTriggerid);

        final File file=File.createTempFile("images","jpg");
        PendingIntent finalNotificationPendingIntent = notificationPendingIntent;
        mImageRef.getFile(file).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                bitmap=BitmapFactory.decodeFile(file.getAbsolutePath());
                Log.e("image", "image  found");
                Log.e("image", "value"+bitmap.toString());
                try {
               if (msg.equals("enter"))
               {

                   addShopToDb(msg, bitmap, geoFenceTriggerid);
                   IncrementCount(geoFenceTriggerid);
               }
               else if(msg.equals("exit"))
                   RemoveShopFromDB(geoFenceTriggerid);
                    Random rd = new Random();
                    notificatioMng.notify(GEOFENCE_NOTIFICATION_ID+rd.nextInt(), createNotification(msg, notificationPendingIntent,state,bitmap));

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("image", "image not found");
            }
        });


    }

    private void IncrementCount(String geoFenceTriggerid)
    {
        Firedb.collection("users").document(geoFenceTriggerid).get().addOnCompleteListener(new
                                                                                                          OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                DocumentSnapshot document = task.getResult();
                String group = (String) document.get("count");
                int count=Integer.parseInt(group);
                count++;
                HashMap<String,Object> GeoHashMap=new HashMap<>();
                GeoHashMap.put("count",String.valueOf(count));
                Firedb.collection("users").document(geoFenceTriggerid).update(GeoHashMap);
                Log.d("ajay", "Count incremented");


            }
        });
    }


    // Create notification
    private Notification createNotification(String msg, PendingIntent notificationPendingIntent, boolean state, Bitmap bitmap) throws IOException
    {
        Log.d("ajay", "notify exit");
        if (!state)
        {
            notificationPendingIntent=null;
        }
        NotificationCompat.BigPictureStyle notiStyle = new NotificationCompat.BigPictureStyle();
        notiStyle.bigPicture(bitmap);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "007");
        notificationBuilder
                .setSmallIcon(R.drawable.ic_action_location)
                .setColor(Color.RED)
                .setContentTitle(msg)
                .setContentText("Geofence Notification!")
                .setContentIntent(notificationPendingIntent)
                .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND)
                .setAutoCancel(true).setStyle(notiStyle);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelId = "007";
            NotificationChannel channel = new NotificationChannel(
                    channelId,
                    "ambulance",
                    NotificationManager.IMPORTANCE_HIGH);
            notificatioMng.createNotificationChannel(channel);
            notificationBuilder.setChannelId(channelId);
        }

        return notificationBuilder.build();
    }




    private static String getErrorString(int errorCode) {
        switch (errorCode) {
            case GeofenceStatusCodes.GEOFENCE_NOT_AVAILABLE:
                return "GeoFence not available";
            case GeofenceStatusCodes.GEOFENCE_TOO_MANY_GEOFENCES:
                return "Too many GeoFences";
            case GeofenceStatusCodes.GEOFENCE_TOO_MANY_PENDING_INTENTS:
                return "Too many pending intents";
            default:
                return "Unknown error.";
        }
    }

    @Override
    public void onDestroy() {
        Log.e("recovercheck", "intent service destroyed");
        super.onDestroy();
    }
}
