package com.tinmegali.mylocation;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import androidx.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;


public class DataBaseHelper extends SQLiteOpenHelper {
        SQLiteDatabase sqLiteDatabase;
        Cursor cursor;
        private static final int DATABASE_VERSION = 2;
        public static final String database_name="geofence";
        public final String table_name="geo_Table";
        public final String col_1="uID";
        public final String col_2="datetime";
        public final String col_3="latlang";
        public final String col_4="Geo_id";
        public final String col_5="transition";


        public DataBaseHelper(@Nullable Context context) {
            super(context, database_name, null, 1);

        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {
            sqLiteDatabase.execSQL("create table "+table_name+"(uID INTEGER PRIMARY KEY AUTOINCREMENT,datetime TEXT,latlang TEXT,Geo_id TEXT,transition TEXT) ");
            Log.e("data","table created");
        }

    /**
     * this method is exeuted when the app is installed or upgraded with the newer version of databse,
     * @param sqLiteDatabase
     * @param i
     * @param i1
     */
        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
            sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+ table_name);
            onCreate(sqLiteDatabase);
            Log.e("data","table updated");
        }


    public List<LogModel> getAllLog() {

            List<LogModel> contactList = new ArrayList<LogModel>();
        LogModel logModel;

        // Select All Query
        String selectQuery = "SELECT  * FROM " + table_name;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do
            {
                logModel=new LogModel();
                logModel.setLogId(cursor.getInt(0));
                logModel.setDateTime(cursor.getString(1));
                logModel.setLati(cursor.getDouble(2));
                logModel.setGeoFenceId(cursor.getString(3));
                logModel.setTransiion(cursor.getString(4));

                Log.e("DBCHECK","Id : "+cursor.getInt(0));
                Log.e("DBCHECK","datetime : "+cursor.getString(1));
                Log.e("DBCHECK","latlong : "+cursor.getString(2));
                Log.e("DBCHECK","geoid : "+cursor.getString(3));
                Log.e("DBCHECK","transition : "+cursor.getString(4));
                Log.e("DBCHECK","--------------------");
                contactList.add(logModel);

            } while (cursor.moveToNext());
        }

        // return contact list
        return contactList;
    }


    public void insertdata(String datetime, LatLng latlang, String Geo_id, String transition )
        {



            //it creates the table with all the fields
//            SQLiteDatabase sqLiteDatabase=this.getReadableDatabase();
//            ContentValues contentValues=new ContentValues();
//            contentValues.put(col_2,datetime );
//            contentValues.put(col_3, String.valueOf(latlang));
//            contentValues.put(col_4,Geo_id);
//            contentValues.put(col_5,transition);
//            long res = sqLiteDatabase.insert(table_name,null,contentValues);
//            Log.e("database","inserted"+res);
        }



}


