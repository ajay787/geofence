package com.tinmegali.mylocation;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;
import java.util.Locale;

public class LogAdapter extends RecyclerView.Adapter<LogAdapter.ViewHolder> {
    private List<LogModel> list1;
    String name="na";

    FirebaseFirestore Firedb;
    private OnRecyclerViewItemClickListener onRecyclerViewItemClickListener;
    public LogAdapter(List<LogModel> list1) {
        this.list1 = list1;
        Firedb = FirebaseFirestore.getInstance();

    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout_log, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        int logid = list1.get(position).getLogId();

        String geoid = list1.get(position).getGeoFenceId();
        String datetime = list1.get(position).getDateTime();
        String trans = list1.get(position).getTransiion();

        holder.tvId.setText(String.valueOf(position));
        if (!trans.equals("enter"))
        {

            holder.tvGeoNear.setText("Not Near You");
        }else
        {
            holder.tvGeoNear.setText("Near You");
        }
        holder.tvDateTime.setText(datetime);

        //ReadName(geoid);
        Firedb.collection("users").document(geoid).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {

            @Override public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                DocumentSnapshot document = task.getResult();
                name = (String) document.get("ShopName");
                Log.e("ajay", "onComplete: "+ (String) document.get("ShopName"));
                holder.Shopname.setText(name);

            }
        });



        byte[] byteArray = list1.get(position).getByteArray();
        Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        holder.ShopImage.setImageBitmap(bmp);



    }

    @Override
    public int getItemCount() {
        return list1.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvId;
        TextView Shopname;
        TextView tvGeoNear;
        TextView tvDateTime;
        TextView tvTransition;
        ImageView ShopImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvId = itemView.findViewById(R.id.tvId1);
            Shopname = itemView.findViewById(R.id.ShopName);
            tvGeoNear = itemView.findViewById(R.id.geoNear);
            tvDateTime = itemView.findViewById(R.id.tvDateTime);
            tvTransition = itemView.findViewById(R.id.tvTransition);
            ShopImage = itemView.findViewById(R.id.ShopImage);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onRecyclerViewItemClickListener != null) {
                        onRecyclerViewItemClickListener.onItemClick(getAdapterPosition(), v);
                    }
                }
            });

        }

    }
    public void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClickListener onRecyclerViewItemClickListener) {
        this.onRecyclerViewItemClickListener = onRecyclerViewItemClickListener;
    }
}

