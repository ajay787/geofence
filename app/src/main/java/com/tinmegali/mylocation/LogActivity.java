package com.tinmegali.mylocation;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class LogActivity extends AppCompatActivity implements  OnRecyclerViewItemClickListener{

    /** Android Views **/
    RelativeLayout parentLayout;
    androidx.recyclerview.widget.RecyclerView recyclerViewLog;
    TextView div1;
    androidx.recyclerview.widget.RecyclerView recyclerViewMarker;
    TextView div2;
    androidx.recyclerview.widget.RecyclerView recyclerViewCircle;
    List<LogModel> logModelList;
    FirebaseFirestore Firedb;
    FirebaseUser user;
    String name="NA";
    boolean success;
    /** Android Views **/

    /**
     * Binds XML views
     * Call this function after setContentView() in onCreate().
     **/
    private void bindViews(){
        parentLayout =  findViewById(R.id.parentLayout);
        recyclerViewLog =  findViewById(R.id.recyclerViewLog);
         user = FirebaseAuth.getInstance().getCurrentUser();

        recyclerViewCircle =  findViewById(R.id.recyclerViewCircle);
        Firedb = FirebaseFirestore.getInstance();
    }


        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_log_layout);
            bindViews();
         //   dataBaseHelper=new DataBaseHelper(this);

            LinearLayoutManager layoutManager=new LinearLayoutManager(this);
            layoutManager.setReverseLayout(true);
            layoutManager.setStackFromEnd(true);
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerViewLog.setLayoutManager(layoutManager);



            List<LogModel> logModelList= LogModel.listAll(LogModel.class);
            List<CustomCircle> circleList= CustomCircle.listAll(CustomCircle.class);
            List<CustomMarker> markerList= CustomMarker.listAll(CustomMarker.class);

            Log.d("ajay","size="+logModelList.size());
            for (int i = 0; i < logModelList.size(); i++) {

                Log.d("ajay",i+" id =  "+logModelList.get(i).getLogId());
            }

            LogAdapter adapter=new LogAdapter(logModelList);
            CircleAdapter adapter1=new CircleAdapter(circleList);
            MarkerAdapter adapter2=new MarkerAdapter(markerList);
            adapter.setOnRecyclerViewItemClickListener(LogActivity.this);


    //        recyclerViewMarker.setAdapter(adapter2);
      //      recyclerViewCircle.setAdapter(adapter1);
            recyclerViewLog.setAdapter(adapter);


        }
    private void IncrementCount(String geoFenceTriggerid)
    {
        Firedb.collection("users").document(geoFenceTriggerid).get().addOnCompleteListener(new
                                                                                                   OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                DocumentSnapshot document = task.getResult();String group = (String) document.get("VisitedCount");
                int count=Integer.parseInt(group);
                count++;
                HashMap<String,Object> GeoHashMap=new HashMap<>();
                GeoHashMap.put("VisitedCount",String.valueOf(count));
                Firedb.collection("users").document(geoFenceTriggerid).update(GeoHashMap);
                Log.d("ajay", "VisitedCount incremented");


            }
        });
    }
    @Override
    public void onItemClick(int position, View view)
    {
        logModelList= LogModel.listAll(LogModel.class);

        LogModel logModel=logModelList.get(position);
        ShopModel shopModel=new ShopModel();
        shopModel.setDateTime(logModel.getDateTime());
        shopModel.setGeoFenceId(logModel.getGeoFenceId());
        shopModel.setLati(logModel.getLati());
        shopModel.setLongi(logModel.getLongi());
        shopModel.setTransiion(logModel.getTransiion());
        shopModel.setVisited("True");
        shopModel.setByteArray(logModel.getByteArray());
        shopModel.setShopType(logModel.getShopType());
        ShopModel.save(shopModel);

        IncrementCount(logModel.getGeoFenceId());

        Firedb.collection("users").document(user.getUid()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                DocumentSnapshot document = task.getResult();
                String group = (String) document.get("pref1");
                if (group.equals("empty"))
                {
                    HashMap<String, Object> GeoHashMap = new HashMap<>();
                    GeoHashMap.put("pref1", String.valueOf(logModel.getShopType()));
                    Firedb.collection("users").document(user.getUid()).update(GeoHashMap);
                }
                else
                {
                     group = (String) document.get("pref2");
                    if (group.equals("empty"))
                    {
                        HashMap<String, Object> GeoHashMap = new HashMap<>();
                        GeoHashMap.put("pref2", String.valueOf(logModel.getShopType()));
                        Firedb.collection("users").document(user.getUid()).update(GeoHashMap);
                    }
                    else
                    {
                        group = (String) document.get("pref3");
                        if (group.equals("empty"))
                        {
                            HashMap<String, Object> GeoHashMap = new HashMap<>();
                            GeoHashMap.put("pref3", String.valueOf(logModel.getShopType()));
                            Firedb.collection("users").document(user.getUid()).update(GeoHashMap);
                        }
                        else
                        {
                            presentInPref(logModel.getShopType());




                        }

                    }


                }
                Log.d("ajay", "VisitedCount incremented");
            }
        });


        String strUri = "http://maps.google.com/maps?q=loc:" + logModelList.get(position).getLati() + "," + logModelList.get(position).getLongi() + " (" + "Label which you want" + ")";
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(strUri));

        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");

        startActivity(intent);

    }

    private boolean presentInPref(String shopType)
    {

        Firedb.collection("users").document(user.getUid()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                DocumentSnapshot document = task.getResult();
                String group1 = (String) document.get("pref1");
                String group2= (String) document.get("pref2");
                String group3= (String) document.get("pref3");
                if (group1.equals(shopType))
                {
                    String Count1 = (String) document.get("count1");

                    int count=Integer.parseInt(Count1);
                    if (count==0)
                        count += 2;
                    else {
                        count++;
                    }
                    HashMap<String,Object> GeoHashMap=new HashMap<>();
                    GeoHashMap.put("count1",String.valueOf(count));
                    Firedb.collection("users").document(user.getUid()).update(GeoHashMap);
                success=true;

                }
                else if (group2.equals(shopType))
                {
                    String Count1 = (String) document.get("count2");

                    int count=Integer.parseInt(Count1);
                    if (count!=0)
                        count += 2;
                    count++;
                    HashMap<String,Object> GeoHashMap=new HashMap<>();
                    GeoHashMap.put("count2",String.valueOf(count));
                    Firedb.collection("users").document(user.getUid()).update(GeoHashMap);
                    success=true;

                }
               else if (group3.equals(shopType))
                {
                    String Count1 = (String) document.get("count3");

                    int count=Integer.parseInt(Count1);
                    if (count!=0)
                        count += 2;
                    count++;
                    HashMap<String,Object> GeoHashMap=new HashMap<>();
                    GeoHashMap.put("count3",String.valueOf(count));
                    Firedb.collection("users").document(user.getUid()).update(GeoHashMap);
                    success=true;



                }
               else
                {
                    HashMap<String, Object> GeoHashMap = new HashMap<>();
                    GeoHashMap.put("pref1", shopType);
                    GeoHashMap.put("count1", "0");
                    Firedb.collection("users").document(user.getUid()).update(GeoHashMap);
                }


            }
        });
       return false;
    }


}


