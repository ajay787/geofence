package com.tinmegali.mylocation;

import android.Manifest;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.auth.User;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class ShopkeeperActivity extends AppCompatActivity
        implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        OnMapReadyCallback,//interface
        GoogleMap.OnMapClickListener,
        GoogleMap.OnMarkerClickListener,
        ResultCallback<Status> {
    private static final String TAG = ShopkeeperActivity.class.getSimpleName();
    private static LatLng mark;
    private List<Marker> geoFenceMarker = new ArrayList<>();
    private Button addd;
    private Button remove;
    private Button updateGeo;
    private GoogleMap map;
    private GoogleApiClient googleApiClient;
    private List<Circle> removecirclelist;
    private Location lastLocation;
    private AppBarConfiguration mAppBarConfiguration;
    FirebaseFirestore Firedb;
    DatabaseReference databaseReference;
    FirebaseUser user;

    int j = 0;
    int k = 1;
    int upp=0;
    public int GEOFENCERADIUS;


    private MapView mapFragment;
    EditText editText;

    private static final String NOTIFICATION_MSG = "NOTIFICATION MSG";
    private ImageView nav_header_imageView;

    // Create a Intent send by the notification
    public static Intent makeNotificationIntent(Context context, String msg) {
        Intent intent = new Intent(context, LogActivity.class);
        intent.putExtra(NOTIFICATION_MSG, msg);
        return intent;
    }
    public static LatLng method() {
        return mark;

    }


    //service class object
    GeofenceTrasitionService gfs;
    //circle list
    List<CustomCircle> circleList;
    //marker list
    List<CustomMarker> listMarker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_navigation);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        Firedb = FirebaseFirestore.getInstance();

        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow)
                .setDrawerLayout(drawer)
                .build();

        View header = navigationView.getHeaderView(0);

        TextView textViewHeaderName = (TextView) header.findViewById(R.id.nav_header_textView);
        nav_header_imageView = (ImageView) header.findViewById(R.id.nav_header_imageView);

         user = FirebaseAuth.getInstance().getCurrentUser();
        //to get the type of the user


        try { Firedb.collection("users").document(user.getUid().toString()).get().addOnCompleteListener(new
                                                                                                              OnCompleteListener<DocumentSnapshot>() {
                                                                                                                  @Override
                                                                                                                  public void onComplete(@NonNull Task<DocumentSnapshot> task) {
            DocumentSnapshot document = task.getResult();
            String group = (String) document.get("Type");
            Log.d("TAG", group);
            Toast.makeText(ShopkeeperActivity.this, "Login for :"+group.toUpperCase(), Toast.LENGTH_LONG).show();

            textViewHeaderName.setText(user.getEmail()+"  "+ group);

                                                                                                                  }
                                                                                                              });


        }catch (Exception e)
        {
            e.printStackTrace();
        }

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId())
                {
                    case R.id.logout:

                        new AlertDialog.Builder(ShopkeeperActivity.this)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setTitle("Logging out!")
                                .setMessage("Are you sure you want to Logout")
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                                {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent=new Intent(ShopkeeperActivity.this,loginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        FirebaseAuth.getInstance().signOut();
                                        startActivity(intent);

                                    }

                                })
                                .setNegativeButton("No", null)
                                .show();
                        break;

                        case R.id.uploadimage:
                            Intent IntentForImage = new Intent(ShopkeeperActivity.this, UploadImageActivity.class);
                            startActivity(IntentForImage);
                            break;

                    case R.id.count:
                        Intent IntentForCount = new Intent(ShopkeeperActivity.this, CountOfCustomer.class);
                        startActivity(IntentForCount);
                        break;

                }

                return false;
            }
        });



        addd = (Button) findViewById(R.id.addd);
        remove = (Button) findViewById(R.id.remove);
        updateGeo= (Button) findViewById(R.id.updateGeo);

        circleList = CustomCircle.listAll(CustomCircle.class);
        listMarker = CustomMarker.listAll(CustomMarker.class);

        gfs = new GeofenceTrasitionService();

        // initialize GoogleMaps
        initGMaps();
        mapFragment.onCreate(savedInstanceState);
        mapFragment.onResume();
        // create GoogleApiClient
        createGoogleApi();

        addd.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {

                j = 1;
                Toast.makeText(ShopkeeperActivity.this, "tab on screen to add marker", Toast.LENGTH_LONG).show();
                //the id of the shop should be the name of the shop

//                FirebaseDatabase database = FirebaseDatabase.getInstance();
//                DatabaseReference myRef = database.getReference("message");
//                myRef.setValue("data");
            }
        });

        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                k++;
                if (k % 2 == 0)
                    remove.setText("cancel");
                else
                    remove.setText("remove");
                Toast.makeText(ShopkeeperActivity.this, "tab on the markers you want to remove ", Toast.LENGTH_LONG).show();
            }
        });
        updateGeo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                updateCount(false);
                upp=100;

            }
        });


    }




    // Create GoogleApiClient instance
    private void createGoogleApi() {
        Log.d(TAG, "createGoogleApi()");
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Call GoogleApiClient connection when starting the Activity
        googleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();

        // Disconnect GoogleApiClient when stopping Activity
        googleApiClient.disconnect();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.navigation, menu);
        return true;
    }
    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }


    private final int REQ_PERMISSION = 999;

    // Check for permission to access Location
    private boolean checkPermission() {
        Log.d(TAG, "checkPermission()");
        // Ask for permission if it wasn't granted yet
        return (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED);
    }

    // Asks for permission
    private void askPermission() {
        Log.d(TAG, "askPermission()");
        ActivityCompat.requestPermissions(
                this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                REQ_PERMISSION
        );
    }

    // Verify user's response of the permission requested
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult()");
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQ_PERMISSION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission granted
                    getLastKnownLocation();


                } else {

                    // Permission denied
                    permissionsDenied();
                }
                break;
            }
        }
    }

    // App cannot work without the permissions
    private void permissionsDenied() {
        Log.w(TAG, "permissionsDenied()");
        // TODO close app and warn user
    }

    // Initialize GoogleMaps
    private void initGMaps() {
        mapFragment = (MapView)findViewById(R.id.map);

        //A GoogleMap must be acquired using getMapAsync(OnMapReadyCallback).
        // This class automatically initializes the maps system and the view.
        mapFragment.getMapAsync(this);

        //it will call on map ready when map is ready to use
    }
    // Callback called when Map is ready


    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "onMapReady()");
        map = googleMap;

        Log.d("mapready", "map is ready");
        MapStyleOptions mapStyleOptions = MapStyleOptions.loadRawResourceStyle(this, R.raw.style);
        map.setMapStyle(mapStyleOptions);

        recoverLastCircles();
        recoverLastMarkers();
        recoverLastGeofences();

        //If you want to respond to a user tapping on a point on the map,
        // you can use an OnMapClickListener
        // which you can set on the map by calling GoogleMap.setOnMapClickListener(OnMapClickListener).
        map.setOnMapClickListener(this);
        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(final Marker marker) {
                Log.d(TAG, "removeGeofenceDraw()");
                if (geoFenceMarker == null) {
                    Toast.makeText(ShopkeeperActivity.this, "empty array list", Toast.LENGTH_LONG).show();
                } else {
                    if (k % 2 == 0) {
                        Log.d(TAG, "clearGeofence()");
                        LocationServices.GeofencingApi.removeGeofences(
                                googleApiClient,
                                createGeofencePendingIntent()
                        ).setResultCallback(new ResultCallback<Status>() {
                            @Override
                            public void onResult(@NonNull Status status) {
                                if (status.isSuccess()) {
                                    // remove drawing
                                    removeGeofenceDraw(marker);
                                    HashMap<String,Object> GeoHashMap=new HashMap<>();
                                    GeoHashMap.put("State","InActive");
                                    Firedb.collection("users").document(user.getUid().toString()).update(GeoHashMap);
                                    updateCount(false);

                                }
                            }
                        });

                        return true;
                    }
                    else if (upp==100)
                    {
                        Log.d(TAG, "UpdateGeofence()");
                        LocationServices.GeofencingApi.removeGeofences(
                                googleApiClient,
                                createGeofencePendingIntent()
                        ).setResultCallback(new ResultCallback<Status>() {
                            @Override
                            public void onResult(@NonNull Status status) {
                                if (status.isSuccess()) {
                                    // remove drawing
                                    removeGeofenceDraw(marker);
                                    LatLng location = new LatLng(marker.getPosition().latitude,marker.getPosition().longitude);
                                    //pass shoopkeeper id from DB
                                    setGeofencebyuser(location,user.getUid());
                                }
                            }
                        });
                        return true;

                    }
                    else {
                        Toast.makeText(ShopkeeperActivity.this, "please press remove button", Toast.LENGTH_LONG).show();
                    }

                }

                return false;

            }
        });
    }







    @Override
    public void onMapClick(LatLng latLng) {
        if (j == 1) {
            Log.d(TAG, "onMapClick(" + latLng + ")");
            setGeofencebyuser(latLng,"");

        } else {
            Toast.makeText(ShopkeeperActivity.this, "press add to insert marker", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        Log.d(TAG, "onMarkerClickListener: " + marker.getPosition());
        return false;
    }


    private LocationRequest locationRequest;
    // Defined in mili seconds.
    // This number in extremely low, and should be used only for debug
    private final int UPDATE_INTERVAL = 3000;
    private final int FASTEST_INTERVAL = 900;

    // Start location Updates
    private void startLocationUpdates() {
        Log.i(TAG, "startLocationUpdates()");
        locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);

        if (checkPermission())
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "onLocationChanged [" + location + "]");
        //we have earlier created a variable to store lastlocation
        //as the location is changed this method should be called
        lastLocation = location;
        databaseReference = FirebaseDatabase.getInstance().getReference();
        databaseReference.child("lat").setValue(lastLocation.getLatitude());
        databaseReference.child("lang").setValue(lastLocation.getLongitude());
        // we will get the updated in location
        //and we will display the longitude and latitude
        writeActualLocation(location);
    }

    // GoogleApiClient.ConnectionCallbacks connected
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i(TAG, "onConnected()");
        getLastKnownLocation();
//        recoverGeofenceMarker();
    }

    // GoogleApiClient.ConnectionCallbacks suspended
    @Override
    public void onConnectionSuspended(int i) {

        Log.w(TAG, "onConnectionSuspended()");
    }

    // GoogleApiClient.OnConnectionFailedListener fail
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.w(TAG, "onConnectionFailed()");
    }

    // Get last known location
    private void getLastKnownLocation() {
        Log.d(TAG, "getLastKnownLocation()");
        if (checkPermission())
        {
            lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            if (lastLocation != null) {
                Log.i(TAG, "LasKnown location. " +
                        "Long: " + lastLocation.getLongitude() +
                        " | Lat: " + lastLocation.getLatitude());
                writeLastLocation();
                startLocationUpdates();
            } else {
                Log.w(TAG, "No location retrieved yet");
                startLocationUpdates();
            }
        } else askPermission();
    }

    private void writeActualLocation(Location location) {

        markerLocation(new LatLng(location.getLatitude(), location.getLongitude()));
    }

    private void writeLastLocation() {

        writeActualLocation(lastLocation);
    }

    private Marker locationMarker;

    private void markerLocation(LatLng latLng) {
        Log.i(TAG, "markerLocation(" + latLng + ")");
        mark = latLng;
        String title = latLng.latitude + ", " + latLng.longitude;
        MarkerOptions markerOptions = new MarkerOptions()
                .position(latLng)
                .title(title);
        if (map != null) {
            if (locationMarker != null)
                locationMarker.remove();
            locationMarker = map.addMarker(markerOptions);
            float zoom = 14f;
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, zoom);
            map.animateCamera(cameraUpdate);
        }
    }


    private Marker geoFenceMarker1;
    private HashMap<Marker, Circle> hashMap = new HashMap<>();
    private Geofence geo;
    private List<Geofence> geolist = new ArrayList<>();
    private List<GeofencingRequest> georelist = new ArrayList<>();
    private String uid = UUID.randomUUID().toString();


    private void markerForGeofence(LatLng latLng, String Id) {
        uid = UUID.randomUUID().toString();
        Log.i(TAG, "markerForGeofence(" + latLng + ")");
        String title = latLng.latitude + ", " + latLng.longitude;
        // Define marker options
        MarkerOptions markerOptions = new MarkerOptions()
                .position(latLng)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
                .title(title);

        if (map != null) {



            //adding marker
            CustomMarker customMarker = new CustomMarker();
            LatLng latLng1 = markerOptions.getPosition();
            geoFenceMarker1 = map.addMarker(markerOptions);
            geoFenceMarker.add(geoFenceMarker1);

            Global.getInstance().getGeofenceTrasitionService().addMarker(geoFenceMarker1);

            Log.e("check ", "added");
            Log.d(TAG, "createGeofence");

            latLng = geoFenceMarker1.getPosition();
            geo = new Geofence.Builder().setRequestId(user.getUid()).
                    setCircularRegion(latLng.latitude, latLng.longitude, GEOFENCERADIUS).
                    setExpirationDuration(Geofence.NEVER_EXPIRE).
                    setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT).build();

            customMarker.setLati(latLng1.latitude);
            customMarker.setLongi(latLng1.longitude);
            customMarker.setGeoFenceId(geo.getRequestId());
            long id = customMarker.save();
            customMarker.setCustomid(id);
            customMarker = CustomMarker.findById(CustomMarker.class, id);
            customMarker.save();


            geolist.add(geo);

            Global.getInstance().getGeofenceTrasitionService().addGeofence(geo);




            GeofencingRequest geofenceRequest = new GeofencingRequest.Builder().setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER)
                    .addGeofence(geo).build();
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            LocationServices.GeofencingApi.addGeofences(
                    googleApiClient,
                    geofenceRequest,
                    createGeofencePendingIntent()
            ).setResultCallback(this);
            georelist.add(geofenceRequest);
            CircleOptions circleOptions = new CircleOptions()
                    .center(geoFenceMarker1.getPosition())
                    .strokeColor(Color.argb(50, 70, 70, 70))
                    .fillColor(Color.argb(100, 150, 150, 150))
                    .radius(GEOFENCERADIUS);

            //circle adding
            CustomCircle customCircle = new CustomCircle();
            customCircle.setLati(latLng1.latitude);
            customCircle.setLongi(latLng1.longitude);
            customCircle.setRadius(GEOFENCERADIUS);
            customCircle.setGeoFenceId(geo.getRequestId());
            customCircle.save();

            circle = map.addCircle(circleOptions);
            removecirclelist.add(circle);
            hashMap.put(geoFenceMarker1, circle);
            HashMap<String,Object> GeoHashMap=new HashMap<>();
            GeoHashMap.put("ID",user.getUid().toString());
            GeoHashMap.put("Latitude",String.valueOf(latLng.latitude));
            GeoHashMap.put("Longitude",String.valueOf(latLng.longitude));
            GeoHashMap.put("Radius",String.valueOf(GEOFENCERADIUS));
            GeoHashMap.put("State","Active");



            Firedb.collection("users").document(user.getUid().toString()).update(GeoHashMap);
            updateCount(true);
            Global.getInstance().getGeofenceTrasitionService().addCircle(circle);




        }
        Toast.makeText(this, "total markers=" + geoFenceMarker.size(), Toast.LENGTH_LONG).show();


    }
    public void updateCount(boolean b) {

            Firedb.collection("Geofence").document("count").get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    DocumentSnapshot document = task.getResult();
                    String geocountString = (String) document.get("count");
                    Log.d("TAG", geocountString);
                    HashMap<String, Object> count = new HashMap<>();
                    int Geocount = Integer.parseInt(geocountString);
                    if (b)
                    {
                        Geocount++;
                    }
                    else
                        Geocount--;
                    count.put("count", String.valueOf(Geocount));
                    Firedb.collection("Geofence").document("count").update(count);

                }
            });


    }
    private static final long GEO_DURATION = 60 * 60 * 1000;
    private PendingIntent geoFencePendingIntent;
    private final int GEOFENCE_REQ_CODE = 0;

    private PendingIntent createGeofencePendingIntent()
    {

        Log.d(TAG, "createGeofencePendingIntent");
        if (geoFencePendingIntent != null)
            return geoFencePendingIntent;

        Intent intent = new Intent(this, GeofenceTrasitionService.class);
        return PendingIntent.getService(this, GEOFENCE_REQ_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }


    @Override
    public void onResult(@NonNull Status status)
    {
        Log.i(TAG, "onResult: " + status);
        if (status.isSuccess())
        {

        }
        else
        {
            // inform about fail
        }
    }


    private final String KEY_GEOFENCE_LAT = "GEOFENCE LATITUDE";
    private final String KEY_GEOFENCE_LON = "GEOFENCE LONGITUDE";

    private Circle circle;
    private LatLng var;

    private void removeGeofenceDraw(Marker marker) {
        Log.d("check", "in method");


        if (listMarker != null) {

            String removeid = null;
            Log.d("check", "first if");


            listMarker = CustomMarker.listAll(CustomMarker.class);
            circleList=CustomCircle.listAll(CustomCircle.class);
            LatLng deleteLatlng=marker.getPosition();
            Toast.makeText(this,"the id is="+deleteLatlng,Toast.LENGTH_LONG).show();


            for (int i = 0; i < listMarker.size(); i++) {

                LatLng latLng = new LatLng(listMarker.get(i).getLati(), listMarker.get(i).getLongi());
                if(deleteLatlng.equals(latLng))
                {
                    Circle circle=removecirclelist.get(i);

                    Log.d("check", "second if");
                    removeid=listMarker.get(i).getGeoFenceId();
                    try {


                        //CustomMarker customMarker = CustomMarker.findById(CustomMarker.class, id);

                        List<CustomMarker> customMarker =  CustomMarker.findWithQuery(CustomMarker.class, "Select * from CUSTOM_MARKER where geo_fence_id = ?", removeid);
                        List<CustomCircle> customCircles=CustomCircle.findWithQuery(CustomCircle.class, "Select * from CUSTOM_MARKER where geo_fence_id = ?", removeid);
                        Log.d("check", "data=  " + customMarker);
                        if(customMarker.size()>0) {
                            customMarker.get(0).delete();
                            customCircles.get(0).delete();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    marker.remove();
                    circle.remove();

                    break;

                    // Toast.makeText(this,"the id is="+removeid,Toast.LENGTH_LONG).show();
                }

            }

        }
    }

    AlertDialog alertDialog;


    /**
     *
     * @param latLng
     */
    public void setGeofencebyuser(final LatLng latLng, final String Id)
    {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("RADIUS");
        builder.setMessage("please enter the radius in meters");
        editText = new EditText(this);
        editText.setInputType(InputType.TYPE_CLASS_NUMBER);
        builder.setView(editText);
        builder.setPositiveButton("submit", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {

                String  txt = editText.getText().toString();
                Toast.makeText(ShopkeeperActivity.this, "radius=" + txt, Toast.LENGTH_LONG).show();
                GEOFENCERADIUS = Integer.parseInt(txt);
                markerForGeofence(latLng,Id);
                alertDialog.dismiss();


            }

        });
        builder.setNegativeButton("cancel", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                Toast.makeText(ShopkeeperActivity.this, "please enter the value", Toast.LENGTH_LONG).show();

            }
        });
        alertDialog = builder.create();
        alertDialog.show();
    }

    public void recoverLastMarkers()
    {

        Log.d("restoreData", "marker size i" + listMarker.size());
        if(listMarker!=null)
        {
            for (int i = 0; i < listMarker.size(); i++)
            {

                LatLng latLng = new LatLng(listMarker.get(i).getLati(), listMarker.get(i).getLongi());
                String title = latLng.latitude + ", " + latLng.longitude;
                MarkerOptions markerOptions = new MarkerOptions()
                        .position(latLng)
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
                        .title(title);
                map.addMarker(markerOptions);

            }
        }

    }

    public void recoverLastCircles()
    {

        Log.e("recovercheck", "circle List : "+circleList.size());
        Log.e("recovercheck", "Marker List : "+listMarker.size());

        if(circleList!=null && listMarker!=null)
        {
            removecirclelist=new ArrayList<>();
            for (int i = 0; i < circleList.size(); i++)
            {

                CircleOptions circleOptions = new CircleOptions()
                        .center(new LatLng(listMarker.get(i).getLati(), listMarker.get(i).getLongi()))
                        .strokeColor(Color.argb(50, 70, 70, 70))
                        .fillColor(Color.argb(100, 150, 150, 150))
                        .radius(circleList.get(i).getRadius());
                circle=map.addCircle(circleOptions);
                removecirclelist.add(circle);

            }
        }
    }

    List<CustomGeofenceData> geofence_list;
    public void recoverLastGeofences() {


    }



}