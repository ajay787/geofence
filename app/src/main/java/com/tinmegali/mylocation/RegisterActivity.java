package com.tinmegali.mylocation;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;

public class RegisterActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;
   public TextView signup;
    public EditText firstName;
    public EditText lastName;
    public  EditText email;
    public EditText passWord;
    public EditText confirmPass;
    public Button register;
    public TextView registerShop;
    TextView loginew;
    FirebaseFirestore Firedb;
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        mAuth = FirebaseAuth.getInstance();
        signup =  findViewById(R.id.signup);
        firstName =  findViewById(R.id.firstName);
        lastName =  findViewById(R.id.lastName);
        email =  findViewById(R.id.email);
        passWord =  findViewById(R.id.passWord);
        Firedb = FirebaseFirestore.getInstance();
        confirmPass =  findViewById(R.id.confirmPass);
        register =  findViewById(R.id.register);
        progressBar=findViewById(R.id.progressbar);
        loginew=findViewById(R.id.loginew);
        registerShop=findViewById(R.id.registerShop);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                    if(validInputs())
                       registerUser();

            }
        });
        loginew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(RegisterActivity.this,loginActivity.class);
                startActivity(intent);
            }
        });
        registerShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(RegisterActivity.this, ShopRegisterActiviity.class);
                startActivity(intent);
            }
        });

    }

    private boolean validInputs()
    {
        if(firstName.getText().toString().isEmpty())
        {firstName.setError("Please enter the name");
        return false;
        }
        if(lastName.getText().toString().isEmpty())
        {lastName.setError("Please enter last name");
        return false;
        }
        if(email.getText().toString().isEmpty())
        {email.setError("Please enter the Email");
            return false;
        }
        if(passWord.getText().toString().isEmpty())
        {passWord.setError("Please enter password");
            return false;
        }
        if(passWord.getText().toString().length()<6)
        {passWord.setError("Please enter at least 6 digits password");
            return false;
        }
        if(confirmPass.getText().toString().isEmpty())
        {
            confirmPass.setError("password is empty");
            return false;
        }
        if (!confirmPass.getText().toString().equals(passWord.getText().toString()))
        {
            confirmPass.setError("password doenst match");
            return false;

        }
        return true;
    }

    private void registerUser()
    {
        ShowProgressbar();
        final String email_id=email.getText().toString();
        String Password=passWord.getText().toString();
        mAuth.createUserWithEmailAndPassword(email_id,Password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task)
            {
                if (task.isSuccessful())
                {
                    hideProgressbar();
                    DocumentReference documentReference =Firedb.collection("users").document(mAuth.getCurrentUser().getUid());
                    HashMap<String,String> userHashMap=new HashMap<>();
                    userHashMap.put("Type","customer");
                    userHashMap.put("pref1","empty");
                    userHashMap.put("pref2","empty");
                    userHashMap.put("pref3","empty");
                    userHashMap.put("count1","0");
                    userHashMap.put("count2","0");
                    userHashMap.put("count3","0");

                    //also store the location of the shop
                    documentReference.set(userHashMap);

                                Toast.makeText(RegisterActivity.this, "successfully Registered", Toast.LENGTH_LONG).show();
                                  Intent intent=new Intent(RegisterActivity.this,MainActivity.class);
                                  startActivity(intent);
                }

                }


        });



    }
    public void ShowProgressbar()
    {
        progressBar.setVisibility(View.VISIBLE);
    }
    public void hideProgressbar()
    {
        progressBar.setVisibility(View.GONE);
    }

}