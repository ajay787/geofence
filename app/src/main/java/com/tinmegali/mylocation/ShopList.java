package com.tinmegali.mylocation;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import java.util.List;

public class ShopList extends AppCompatActivity{

    /** Android Views **/
    RelativeLayout parentLayout;
    androidx.recyclerview.widget.RecyclerView recyclerViewLog;

    /** Android Views **/

    /**
     * Binds XML views
     * Call this function after setContentView() in onCreate().
     **/
    private void bindViews(){
        parentLayout =  findViewById(R.id.parentLayout);
        recyclerViewLog =  findViewById(R.id.recyclerViewLog);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_list);
        bindViews();
        //   dataBaseHelper=new DataBaseHelper(this);

        LinearLayoutManager layoutManager=new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewLog.setLayoutManager(layoutManager);




        //List<LogModel> logModelList = LogModel.findWithQuery(LogModel.class, "SELECT * FROM LOG_MODEL WHERE Visited = ?", "True");
        List<ShopModel> ShopModelList= ShopModel.listAll(ShopModel.class);



        ShopListAdapter adapter=new ShopListAdapter(ShopModelList);

        recyclerViewLog.setAdapter(adapter);


    }
}


