package com.tinmegali.mylocation;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static com.tinmegali.mylocation.AppConfig.PROXIMITY_RADIUS;

public class MainActivity extends AppCompatActivity
        implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        OnMapReadyCallback,//interface
        GoogleMap.OnMapClickListener,
        GoogleMap.OnMarkerClickListener,
        ResultCallback<Status> {
    private static final String TAG = MainActivity.class.getSimpleName();
    private static LatLng mark;
    private List<Marker> geoFenceMarker = new ArrayList<>();
    Spinner dropdown;
    private static final String GOOGLE_API_KEY = "AIzaSyB8Xbm7jAj2xw4Jd-TbyWlOiwPxricBIdA";

private Button refresh;
    private GoogleMap map;
    private GoogleApiClient googleApiClient;
    private List<Circle> removecirclelist;
    List<CustomGeofenceData> geofence_list;
    private Location lastLocation;
    private AppBarConfiguration mAppBarConfiguration;
    FirebaseFirestore Firedb;
    DatabaseReference databaseReference;
    AlertDialog alertDialog;

    int j = 0;
    int k = 1;
    int upp=0;
    public int GEOFENCERADIUS;


    private MapView mapFragment;
    EditText editText;

    private static final String NOTIFICATION_MSG = "NOTIFICATION MSG";
    private ImageView nav_header_imageView;

    // Create a Intent send by the notification
    public static Intent makeNotificationIntent(Context context, String msg) {
        Intent intent = new Intent(context, LogActivity.class);
        intent.putExtra(NOTIFICATION_MSG, msg);
        return intent;
    }

    public static LatLng method() {
        return mark;

    }

    //service class object
    GeofenceTrasitionService gfs;
    //circle list
    List<CustomCircle> circleList;
    //marker list
    List<CustomMarker> listMarker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_customer);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        dropdown = findViewById(R.id.spinner1);
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow)
                .setDrawerLayout(drawer)
                .build();

        View header = navigationView.getHeaderView(0);

        TextView textViewHeaderName = (TextView) header.findViewById(R.id.nav_header_textView);
        nav_header_imageView = (ImageView) header.findViewById(R.id.nav_header_imageView);
        Firedb = FirebaseFirestore.getInstance();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        refresh=findViewById(R.id.refresh);
        //to get the type of the user


        textViewHeaderName.setText(user.getEmail());

        dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                if (!dropdown.getItemAtPosition(position).equals("Select Type")) {
                    GEOFENCERADIUS = 0;
                    map.clear();
                    ShowAlert(position);
                }
                map.clear();


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId())
                    {
                        case R.id.logout:

                            new AlertDialog.Builder(MainActivity.this)
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .setTitle("Logging out!")
                                    .setMessage("Are you sure you want to Logout")
                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                                    {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Intent intent=new Intent(MainActivity.this,loginActivity.class);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            FirebaseAuth.getInstance().signOut();
                                            startActivity(intent);

                                        }

                                    })
                                    .setNegativeButton("No", null)
                                    .show();
                            break;



                            case R.id.NearByShops:
                                Intent IntentForSHops = new Intent(MainActivity.this, LogActivity.class);
                                startActivity(IntentForSHops);
                                break;

                                case R.id.Visited:
                                    Intent IntentForVisited = new Intent(MainActivity.this, ShopList.class);
                                    startActivity(IntentForVisited);
                                    break;
                                case R.id.RecomShops:
                                    Intent IntentForRecommended = new Intent(MainActivity.this, RecommendedShops.class);
                                    startActivity(IntentForRecommended);
                                    break;

                    }

                return false;
            }
        });



refresh.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v)
    {
        RenderGeofence();
    }
});


        circleList = CustomCircle.listAll(CustomCircle.class);
        listMarker = CustomMarker.listAll(CustomMarker.class);

        gfs = new GeofenceTrasitionService();

        // initialize GoogleMaps
        initGMaps();
        mapFragment.onCreate(savedInstanceState);
        mapFragment.onResume();
        // create GoogleApiClient

        createGoogleApi();





    }

    private void ShowGoogleShops(int position, int GEOFENCERADIUS)
    {
        StringBuilder googlePlacesUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        googlePlacesUrl.append("location=" + lastLocation.getLatitude() + "," + lastLocation.getLongitude());
        googlePlacesUrl.append("&radius=" + GEOFENCERADIUS);
        googlePlacesUrl.append("&types=" + dropdown.getItemAtPosition(position));
        googlePlacesUrl.append("&sensor=true");
        googlePlacesUrl.append("&key=" + GOOGLE_API_KEY);

        GooglePlacesReadTask googlePlacesReadTask = new GooglePlacesReadTask();
        Object[] toPass = new Object[2];
        toPass[0] = map;
        toPass[1] = googlePlacesUrl.toString();
        googlePlacesReadTask.execute(toPass);
    }

    private int ShowAlert(int position) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("RADIUS");
        builder.setMessage("please enter the radius in meters");
        editText = new EditText(this);
        editText.setInputType(InputType.TYPE_CLASS_NUMBER);
        builder.setView(editText);
        builder.setPositiveButton("submit", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {

                String  txt = editText.getText().toString();
                Toast.makeText(MainActivity.this, "radius=" + txt, Toast.LENGTH_LONG).show();
                GEOFENCERADIUS = Integer.parseInt(txt);
                ShowGoogleShops(position,GEOFENCERADIUS);
                CheckForType(position,GEOFENCERADIUS);

                alertDialog.dismiss();


            }

        });
        builder.setNegativeButton("cancel", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                Toast.makeText(MainActivity.this, "please enter the value", Toast.LENGTH_LONG).show();
                GEOFENCERADIUS=0;

            }
        });
        alertDialog = builder.create();
        alertDialog.show();
        return  GEOFENCERADIUS;

    }

    private void CheckForType(int position, int Radius)
    {
        Log.d("ajay", "Inside CheckForType : ");
        String Type= (String) dropdown.getItemAtPosition(position);
        Log.d("ajay", "type : "+ Type+"Radius="+Radius);
        geofence_list=new ArrayList<>();
        Firedb.collection("users").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful())
                {


                    geofence_list=new ArrayList<>();
                    for (QueryDocumentSnapshot document : task.getResult())
                    {
                        if (document.get("Type").equals("Shop"))
                        {
                            if (document.get("ShopType").equals(Type))
                            {
                                Log.d("ajay", "Type Found");
                                CustomGeofenceData customGeofenceData = new CustomGeofenceData();
                                customGeofenceData.setGeofenceId(document.get("ID").toString());
                                customGeofenceData.setLati(Double.parseDouble(document.get("Latitude").toString()));
                                customGeofenceData.setLongi(Double.parseDouble(document.get("Longitude").toString()));
                                customGeofenceData.setName((String) document.get("ShopName"));
                                geofence_list.add(customGeofenceData);
                            }


                        }
                        Log.d("ajay", "getting Geofence: "+geofence_list.size());

                    }

                    CheckInsideRadius(Radius , geofence_list);



                } else {
                    Log.d(TAG, "Error getting documents: ", task.getException());
                }

            }
        });


    }

    private void CheckInsideRadius(int radius, List<CustomGeofenceData> geofence_list)
    {
        boolean inRange = false;
        if (geofence_list.size()!=0 && radius!=0)
        {
             inRange = false;
            for (int i = 0; i <geofence_list.size() ; i++)
            {
                double distance=getDistanceMeters(new LatLng(geofence_list.get(i).getLati(),geofence_list.get(i).getLongi()),new LatLng(lastLocation.getLatitude(),lastLocation.getLongitude()));
                inRange = (distance <= radius);
                if (inRange)
                {
                    Log.d("ajay", "inside Radius");
                    MarkerOptions markerOptions = new MarkerOptions()
                            .position(new LatLng(geofence_list.get(i).getLati(),geofence_list.get(i).getLongi())).
                                    icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                            .title(geofence_list.get(i).getName().toString());
                    if (map != null) {
                        map.addMarker(markerOptions);
                    }

                }

            }
        }






    }
    public static double getDistanceMeters(LatLng pt1, LatLng pt2){
        double distance = 0d;
        try{
            double theta = pt1.longitude - pt2.longitude;
            double dist = Math.sin(Math.toRadians(pt1.latitude)) * Math.sin(Math.toRadians(pt2.latitude))
                    + Math.cos(Math.toRadians(pt1.latitude)) * Math.cos(Math.toRadians(pt2.latitude))
                    * Math.cos(Math.toRadians(theta));

            dist = Math.acos(dist);
            dist = Math.toDegrees(dist);
            distance = dist * 60 * 1853.1596;
        }
        catch (Exception ex){
            Log.e("ERROR", ex.getMessage());
        }
        return distance;
    }

    private void RenderGeofence()
    {

        geofence_list=new ArrayList<>();

        Firedb.collection("users").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful())
                {


                    geofence_list=new ArrayList<>();
                    for (QueryDocumentSnapshot document : task.getResult())
                    {
                        if (document.get("Type").equals("Shop"))
                        {
                            CustomGeofenceData customGeofenceData = new CustomGeofenceData();
                            customGeofenceData.setGeofenceId(document.get("ID").toString());
                            customGeofenceData.setLati(Double.parseDouble(document.get("Latitude").toString()));
                            customGeofenceData.setLongi(Double.parseDouble(document.get("Longitude").toString()));
                            customGeofenceData.setRadius(Double.parseDouble(document.get("Radius").toString()));
                            geofence_list.add(customGeofenceData);


                        }
                        Log.d("ajay", "getting Geofence: "+geofence_list.size());

                    }



                } else {
                    Log.d(TAG, "Error getting documents: ", task.getException());
                }
                if (geofence_list.size()!=0)
                    SetShopGeofence(geofence_list);
            }
        });
    }

    @SuppressLint("MissingPermission")
    private void SetShopGeofence(List<CustomGeofenceData> geofence_list)
    {
         Geofence geofence;
         createGoogleApi();
        for (int i = 0; i <geofence_list.size() ; i++) {


            geofence = new Geofence.Builder().setRequestId(geofence_list.get(i).getGeofenceId()).
                    setCircularRegion(geofence_list.get(i).getLati(),geofence_list.get(i).getLongi(), (float) geofence_list.get(i).getRadius()).
                    setExpirationDuration(Geofence.NEVER_EXPIRE).
                    setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT).build();

            GeofencingRequest geofenceRequest = new GeofencingRequest.Builder().setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER)
                    .addGeofence(geofence).build();


            LocationServices.GeofencingApi.addGeofences(
                    googleApiClient,
                    geofenceRequest,
                    createGeofencePendingIntent()
            ).setResultCallback(this);
            Log.d("ajay", " Geofences added ");

        }
    }

    // Create GoogleApiClient instance
    private void createGoogleApi() {
        Log.d(TAG, "createGoogleApi()");
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    @Override
    protected void onStart()
    {
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
        super.onStart();

        // Call GoogleApiClient connection when starting the Activity

    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onResume()
    {
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.navigation, menu);
        return true;
    }
    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }


    private final int REQ_PERMISSION = 999;

    // Check for permission to access Location
    private boolean checkPermission() {
        Log.d(TAG, "checkPermission()");
        // Ask for permission if it wasn't granted yet
        return (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED);
    }

    // Asks for permission
    private void askPermission() {
        Log.d(TAG, "askPermission()");
        ActivityCompat.requestPermissions(
                this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                REQ_PERMISSION
        );
    }

    // Verify user's response of the permission requested
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult()");
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQ_PERMISSION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission granted
                    getLastKnownLocation();


                } else {

                    // Permission denied
                    permissionsDenied();
                }
                break;
            }
        }
    }

    // App cannot work without the permissions
    private void permissionsDenied() {
        Log.w(TAG, "permissionsDenied()");
        // TODO close app and warn user
    }

    // Initialize GoogleMaps
    private void initGMaps() {
        mapFragment = (MapView)findViewById(R.id.map);

        //A GoogleMap must be acquired using getMapAsync(OnMapReadyCallback).
        // This class automatically initializes the maps system and the view.
        mapFragment.getMapAsync(this);

        //it will call on map ready when map is ready to use
    }
    // Callback called when Map is ready


    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "onMapReady()");
        map = googleMap;

        Log.d("mapready", "map is ready");
        MapStyleOptions mapStyleOptions = MapStyleOptions.loadRawResourceStyle(this, R.raw.style);
        map.setMapStyle(mapStyleOptions);



        map.setOnMapClickListener(this);
    }







    @Override
    public void onMapClick(LatLng latLng) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        Log.d(TAG, "onMarkerClickListener: " + marker.getPosition());
        return false;
    }


    private LocationRequest locationRequest;
    // Defined in mili seconds.
    // This number in extremely low, and should be used only for debug
    private final int UPDATE_INTERVAL = 3000;
    private final int FASTEST_INTERVAL = 900;

    // Start location Updates
    private void startLocationUpdates() {
        Log.i(TAG, "startLocationUpdates()");
        locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);

        if (checkPermission())
            if (googleApiClient.isConnected())
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }


    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "onLocationChanged [" + location + "]");
        //we have earlier created a variable to store lastlocation
        //as the location is changed this method should be called
        lastLocation = location;
        databaseReference = FirebaseDatabase.getInstance().getReference();
        databaseReference.child("lat").setValue(lastLocation.getLatitude());
        databaseReference.child("lang").setValue(lastLocation.getLongitude());
        // we will get the updated in location
        //and we will display the longitude and latitude
        writeActualLocation(location);
    }

    // GoogleApiClient.ConnectionCallbacks connected
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i("ajay", "onConnected()");
        googleApiClient.connect();
        getLastKnownLocation();

//        recoverGeofenceMarker();
    }

    // GoogleApiClient.ConnectionCallbacks suspended
    @Override
    public void onConnectionSuspended(int i) {

        Log.w(TAG, "onConnectionSuspended()");
    }

    // GoogleApiClient.OnConnectionFailedListener fail
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.w(TAG, "onConnectionFailed()");
    }

    // Get last known location
    private void getLastKnownLocation() {
        Log.d(TAG, "getLastKnownLocation()");
        if (checkPermission())
        {
            lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            if (lastLocation != null) {
                Log.i(TAG, "LasKnown location. " +
                        "Long: " + lastLocation.getLongitude() +
                        " | Lat: " + lastLocation.getLatitude());
                writeLastLocation();
                startLocationUpdates();
            } else {
                Log.w(TAG, "No location retrieved yet");
                startLocationUpdates();
            }
        } else askPermission();
    }

    private void writeActualLocation(Location location) {

        markerLocation(new LatLng(location.getLatitude(), location.getLongitude()));
    }

    private void writeLastLocation() {

        writeActualLocation(lastLocation);
    }

    private Marker locationMarker;

    private void markerLocation(LatLng latLng) {
        Log.i(TAG, "markerLocation(" + latLng + ")");
        mark = latLng;
        String title = latLng.latitude + ", " + latLng.longitude;
        MarkerOptions markerOptions = new MarkerOptions()
                .position(latLng)
                .title(title);
        if (map != null) {
            if (locationMarker != null)
                locationMarker.remove();
            locationMarker = map.addMarker(markerOptions);
            float zoom = 14f;
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, zoom);
            map.animateCamera(cameraUpdate);
        }
    }


    private Marker geoFenceMarker1;
    private HashMap<Marker, Circle> hashMap = new HashMap<>();
    private Geofence geo;
    private List<Geofence> geolist = new ArrayList<>();
    private List<GeofencingRequest> georelist = new ArrayList<>();
    private String uid = UUID.randomUUID().toString();


    private static final long GEO_DURATION = 60 * 60 * 1000;
    private PendingIntent geoFencePendingIntent;
    private final int GEOFENCE_REQ_CODE = 0;

    private PendingIntent createGeofencePendingIntent()
    {

        Log.d(TAG, "createGeofencePendingIntent");
        if (geoFencePendingIntent != null)
            return geoFencePendingIntent;

        Intent intent = new Intent(this, GeofenceTrasitionService.class);
        return PendingIntent.getService(this, GEOFENCE_REQ_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }


    @Override
    public void onResult(@NonNull Status status)
    {
        Log.i(TAG, "onResult: " + status);
        if (status.isSuccess())
        {

        }
        else
            {
            // inform about fail
        }
    }



}