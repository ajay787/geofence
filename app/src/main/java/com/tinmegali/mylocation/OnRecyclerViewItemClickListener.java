package com.tinmegali.mylocation;

import android.view.View;

public interface OnRecyclerViewItemClickListener
{
    public void onItemClick(int position, View view);
}
