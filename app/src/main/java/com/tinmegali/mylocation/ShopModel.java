package com.tinmegali.mylocation;

import com.orm.SugarRecord;

public class ShopModel extends SugarRecord {

    int logId;
    String geoFenceId;
    double lati;
    double longi;

    String dateTime;

    public String getShopType() {
        return ShopType;
    }

    public void setShopType(String shopType) {
        ShopType = shopType;
    }

    String transiion;
    String ShopType;
    public byte[] getByteArray() {
        return byteArray;
    }

    public void setByteArray(byte[] byteArray) {
        this.byteArray = byteArray;
    }

    byte[] byteArray;
    public String getVisited() {
        return VISITED;
    }

    public void setVisited(String VISITED) {
        this.VISITED = VISITED;
    }

    public String VISITED;

    public int getLogId() {
        return logId;
    }

    public void setLogId(int logId) {
        this.logId = logId;
    }

    public String getGeoFenceId() {
        return geoFenceId;
    }

    public void setGeoFenceId(String geoFenceId) {
        this.geoFenceId = geoFenceId;
    }

    public double getLati() {
        return lati;
    }

    public void setLati(double lati) {
        this.lati = lati;
    }

    public double getLongi() {
        return longi;
    }

    public void setLongi(double longi) {
        this.longi = longi;
    }
    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getTransiion() {
        return transiion;
    }

    public void setTransiion(String transiion) {
        this.transiion = transiion;
    }
}
