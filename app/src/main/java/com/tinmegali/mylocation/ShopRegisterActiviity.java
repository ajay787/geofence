package com.tinmegali.mylocation;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;

public  class ShopRegisterActiviity extends AppCompatActivity {
    private FirebaseAuth mAuth;
    TextView signup;
    EditText ShopName;
    EditText OwnerName;
    EditText Email;
    Button pinLocation;
    EditText passWord;
    EditText confirmPass;
    ProgressBar progressbar;
    Button register;
    TextView loginew;
    FirebaseFirestore Firedb;
    String lat;
    String lang;
    Spinner dropdown;
    private String shopType;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_register_activiity);
         Firedb = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        signup =  findViewById(R.id.signup);
        ShopName =  findViewById(R.id.ShopName);
        OwnerName =  findViewById(R.id.OwnerName);
        Email =  findViewById(R.id.MobileNo);
        pinLocation =  findViewById(R.id.pinLocation);
        passWord =  findViewById(R.id.passWord);
        confirmPass =  findViewById(R.id.confirmPass);
        progressbar =  findViewById(R.id.progressbar);
        register =  findViewById(R.id.register);
        loginew =  findViewById(R.id.loginew);
     dropdown = findViewById(R.id.spinner1);
       shopType= (String) dropdown.getSelectedItem();




        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(validInputs())
                    registerUser();

            }
        });
        loginew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ShopRegisterActiviity.this,loginActivity.class);
                startActivity(intent);
            }
        });
        pinLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ShopRegisterActiviity.this,pinLocation.class);
                startActivityForResult(intent,10);
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 10) {
            if(resultCode == Activity.RESULT_OK){
                 lat=data.getStringExtra("lat");
                 lang=data.getStringExtra("lang");
                Toast.makeText(ShopRegisterActiviity.this, "locations of shop:"+lat+"::"+lang, Toast.LENGTH_LONG).show();

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                // Write your code if there's no result
            }
        }
    }

    private boolean validInputs()
    {
        if(ShopName.getText().toString().isEmpty())
        {ShopName.setError("Please enter the Shop name");
            return false;
        }
        if(OwnerName.getText().toString().isEmpty())
        {OwnerName.setError("Please enter Owner name");
            return false;
        }
        if(Email.getText().toString().isEmpty())
        {Email.setError("Please enter the Email");
            return false;
        }
        if(passWord.getText().toString().isEmpty())
        {passWord.setError("Please enter password");
            return false;
        }
        if(passWord.getText().toString().length()<6)
        {passWord.setError("Please enter at least 6 digits password");
            return false;
        }
        if(confirmPass.getText().toString().isEmpty())
        {
            confirmPass.setError("password is empty");
            return false;
        }
        if (!confirmPass.getText().toString().equals(passWord.getText().toString()))
        {
            confirmPass.setError("password doenst match");
            return false;

        }
        return true;
    }

    private void registerUser()
    {
        ShowProgressbar();
        final String email_id=Email.getText().toString();
        String Password=passWord.getText().toString();
        mAuth.createUserWithEmailAndPassword(email_id,Password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {


            @Override
            public void onComplete(@NonNull Task<AuthResult> task)
            {
                if (task.isSuccessful())
                {
                    hideProgressbar();
                    DocumentReference documentReference=Firedb.collection("users").document(mAuth.getCurrentUser().getUid());
                    HashMap<String,String> userHashMap=new HashMap<>();
                    userHashMap.put("Type","Shop");
                    userHashMap.put("lat",lat.toString());
                    userHashMap.put("lang",lang.toString());
                    userHashMap.put("ShopName",ShopName.getText().toString());
                    userHashMap.put("count","0");
                    userHashMap.put("VisitedCount","0");
                    userHashMap.put("ShopType", (String) dropdown.getSelectedItem());
                    Log.e("ajay", "onComplete: "+lang+lat );
                    //also store the location of the shop
                    documentReference.set(userHashMap);
                    Toast.makeText(ShopRegisterActiviity.this, "successfully Registered", Toast.LENGTH_LONG).show();
                    Intent intent=new Intent(ShopRegisterActiviity.this,ShopkeeperActivity.class);
                    startActivity(intent);


                }

            }


        });



    }
    public void ShowProgressbar()
    {
        progressbar.setVisibility(View.VISIBLE);
    }
    public void hideProgressbar()
    {
        progressbar.setVisibility(View.GONE);
    }

}