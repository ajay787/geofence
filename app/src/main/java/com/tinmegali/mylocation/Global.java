package com.tinmegali.mylocation;

import android.app.Application;
import android.content.Context;

import com.orm.SugarContext;

public class Global extends Application {

    private GeofenceTrasitionService geofenceTrasitionService;

    public GeofenceTrasitionService getGeofenceTrasitionService() {

        if(null == geofenceTrasitionService)
            geofenceTrasitionService = new GeofenceTrasitionService();

        return geofenceTrasitionService;
    }

    public void setGeofenceTrasitionService(GeofenceTrasitionService geofenceTrasitionService) {
        this.geofenceTrasitionService = geofenceTrasitionService;
    }

    private static Context appContext;
    private static Global INSTANCE = null;
    @Override
    public void onCreate() {
        super.onCreate();
        appContext = getApplicationContext();

        SugarContext.init(this);
        /* If you has other classes that need context object to initialize when application is created,
         you can use the appContext here to process. */
    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }
    public static Global getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Global();
        }
        return (INSTANCE);
    }

}