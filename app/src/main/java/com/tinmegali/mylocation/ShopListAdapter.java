package com.tinmegali.mylocation;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

public class ShopListAdapter extends RecyclerView.Adapter<ShopListAdapter.ViewHolder> {
    private List<ShopModel> list1;
    FirebaseFirestore Firedb;

    public ShopListAdapter(List<ShopModel> list1) {
        this.list1 = list1;
        Firedb = FirebaseFirestore.getInstance();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout_log, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        int logid = list1.get(position).getLogId();
        String geoid = list1.get(position).getGeoFenceId();
        double latlang = list1.get(position).getLati();
        String datetime = list1.get(position).getDateTime();
        String trans = list1.get(position).getTransiion();

        holder.tvId.setText(String.valueOf(logid));

        holder.tvDateTime.setText(datetime);
        holder.tvTransition.setText("");
        Firedb.collection("users").document(geoid).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {

            @Override public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                DocumentSnapshot document = task.getResult();
                String name = (String) document.get("ShopName");
                Log.e("ajay", "onComplete: "+ (String) document.get("ShopName"));
                holder.ShopName.setText(name);

            }
        });
        byte[] byteArray = list1.get(position).getByteArray();
        Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        holder.ShopImage.setImageBitmap(bmp);

    }

    @Override
    public int getItemCount() {
        return list1.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvId;
        TextView ShopName;
        TextView tvGeoId;
        TextView tvDateTime;
        TextView tvTransition;
        ImageView ShopImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvId = itemView.findViewById(R.id.tvId1);
            ShopName = itemView.findViewById(R.id.ShopName);
            tvGeoId = itemView.findViewById(R.id.geoNear);
            tvDateTime = itemView.findViewById(R.id.tvDateTime);
            tvTransition = itemView.findViewById(R.id.tvTransition);
            ShopImage = itemView.findViewById(R.id.ShopImage);
        }

    }
}

