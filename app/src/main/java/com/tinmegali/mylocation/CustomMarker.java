package com.tinmegali.mylocation;


import androidx.annotation.NonNull;

import com.orm.SugarRecord;

public class CustomMarker extends SugarRecord {

    String geoFenceId;


    public double lati,longi; public long customid;


    public void setCustomid(long customid) {
        this.customid = customid;
    }

    public long getCustomid() {
        return customid;
    }



    public String getGeoFenceId() {
        return geoFenceId;
    }

    public void setGeoFenceId(String geoFenceId) {
        this.geoFenceId = geoFenceId;
    }

    public double getLati() {
        return lati;
    }

    public void setLati(double lati) {
        this.lati = lati;
    }

    public double getLongi() {
        return longi;
    }

    public void setLongi(double longi) {
        this.longi = longi;
    }

    @NonNull
    @Override
    public String toString() {
        return "id-"+customid+" \n geofence id "+geoFenceId+" \nlat lang  "+lati+" .. "+longi;
    }
}
