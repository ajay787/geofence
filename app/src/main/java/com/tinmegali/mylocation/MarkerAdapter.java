package com.tinmegali.mylocation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MarkerAdapter extends RecyclerView.Adapter<MarkerAdapter.ViewHolder> {
    private List<CustomMarker> list1;

    public MarkerAdapter(List<CustomMarker> list1) {
        this.list1 = list1;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout_log, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.tvId.setText("GpeId : "+ list1.get(position).getGeoFenceId());
        holder.tvLati.setText("Latlong : "+list1.get(position).getLati()+ " , "+list1.get(position).getLongi());

    }

    @Override
    public int getItemCount() {
        return list1.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvId;
        TextView tvLati;
        TextView tvGeoId;
        TextView tvDateTime;
        TextView tvTransition;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvId = itemView.findViewById(R.id.tvId1);
            tvLati = itemView.findViewById(R.id.ShopName);
            tvGeoId = itemView.findViewById(R.id.geoNear);
            tvDateTime = itemView.findViewById(R.id.tvDateTime);
            tvTransition = itemView.findViewById(R.id.tvTransition);
        }

    }
}

